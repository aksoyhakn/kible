package com.example.win10.kible.view;

import com.google.android.gms.maps.model.LatLng;

public interface IHaritaView {

    void statusCompass();
    void showProgress();
    void hideProgress();
    void showDialog();
    void showMapType(int x);
    void moveCamera(LatLng latLng,float getBearing);
    void showDistance(LatLng latLng);


}
