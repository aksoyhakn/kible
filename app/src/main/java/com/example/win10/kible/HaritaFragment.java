package com.example.win10.kible;


import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.example.win10.kible.help.Common;
import com.example.win10.kible.presenter.HaritaPre;
import com.example.win10.kible.presenter.ınterface.IHaritaPre;
import com.example.win10.kible.view.IHaritaView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;


/**
 * A simple {@link Fragment} subclass.
 */
public class HaritaFragment extends Fragment implements
        OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener, SensorEventListener, IHaritaView {


    private SupportMapFragment mapFragment;

    private Polyline polyline;
    private IHaritaPre haritaPre;

    private static final int MY_PERMİSSİON_REQUEST_CODE = 7000;
    private static final int PLAY_SERVİCE_RES_REQUEST = 7001;

    private static final int UPDATE_INTERVAL = 5000;
    private static final int FATEST_INTERVAL = 3000;
    private static final int DISPLACEMENT = 10;


    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;

    private GoogleMap mMap;
    private Marker mUserMarker,mKıbleMarker;
    private TextView txtDistance, txtMyDegree;
    private ImageView imgCompass, imgMapType, imgKibleLocation, imgMyLocation;
    private SensorManager sensorManager;
    private Sensor compass;
    private int i = 1;
    private float degree = 0f;
    private RelativeLayout relativeLayout;
    private ProgressBar pbLoad;


    public HaritaFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_harita, container, false);

        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        Common.Kible = new LatLng(21.42252, 39.82621);

        haritaPre = new HaritaPre(this, getActivity());

        //Sensor
        sensorManager = (SensorManager) getActivity().getSystemService(Context.SENSOR_SERVICE);

        relativeLayout = view.findViewById(R.id.relativeLayout);
        pbLoad = view.findViewById(R.id.pbLoad);
        txtDistance = view.findViewById(R.id.txtDistance);
        txtMyDegree = view.findViewById(R.id.txtMyDegree);
        imgCompass = view.findViewById(R.id.imgCompass);

        haritaPre.statusGPS();

        //KıbleLocation
        imgKibleLocation = view.findViewById(R.id.imgKibleLocation);
        imgKibleLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(Common.Kible, 4.0f));
            }
        });

        //MyLocation
        imgMyLocation = view.findViewById(R.id.imgMyLocation);
        imgMyLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(Common.me, 17.0f));

            }
        });

        //MapType
        imgMapType = view.findViewById(R.id.imgMapType);
        imgMapType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showMapType(++i);
            }
        });

        setUpLocation();

        return view;
    }

    @Override
    public void statusCompass() {
        compass = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);

        if (compass != null) {
            sensorManager.registerListener(this, compass, SensorManager.SENSOR_DELAY_NORMAL);
            imgCompass.setVisibility(View.GONE);
        } else {
            showDialog();
        }

    }

    @Override
    public void showProgress() {
        pbLoad.setVisibility(View.VISIBLE);
        relativeLayout.setVisibility(View.INVISIBLE);
    }

    @Override
    public void hideProgress() {
        pbLoad.setVisibility(View.GONE);
        relativeLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void showDialog() {

        imgCompass.setVisibility(View.VISIBLE);


        new AlertDialog.Builder(getContext(), R.style.MyAlertDialogStyle)
                .setTitle(Html.fromHtml("<font color='#48984B'>Pusula sensör bulunamadı</font>"))
                .setMessage("Cihazınızda pusula sensörü bulunmadığı için yönlendirme yapamıyoruz.Fakat mavi çizgi yönü binanızın" +
                        " Kıble tarafını göstermektedir.")
                .setPositiveButton("Tamam", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    public void showMapType(int x) {
        switch (x) {

            case 2:
                mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                imgMapType.setImageResource(R.drawable.map_type_2);
                break;
            case 3:
                mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                imgMapType.setImageResource(R.drawable.map_type_3);
                break;
            default:
                mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                imgMapType.setImageResource(R.drawable.map_type_1);
                i = 1;
        }
    }

    @Override
    public void moveCamera(LatLng latLng, float getBearing) {

        if (polyline != null)
            polyline.remove();


        //Add Marker
        if (mUserMarker != null)
            mUserMarker.remove();

        if (getBearing >= 160f && getBearing <= 180f) {

            polyline = mMap.addPolyline(new PolylineOptions()
                    .clickable(true)
                    .color(Color.GREEN)
                    .add(latLng).add(Common.Kible));

            mUserMarker = mMap.addMarker(new MarkerOptions()
                    .position(latLng)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_green)));

        } else {

            polyline = mMap.addPolyline(new PolylineOptions()
                    .clickable(true)
                    .color(Color.BLUE)
                    .add(latLng).add(Common.Kible));

            mUserMarker = mMap.addMarker(new MarkerOptions()
                    .position(latLng)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_blue)));
        }


        CameraPosition camera = new CameraPosition.Builder()
                .target(latLng)
                .zoom(15f)  //limite ->21f
                .bearing(getBearing) // 0 - 360
                .tilt(35) // limite ->90
                .build();

        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(camera));

        mKıbleMarker = mMap.addMarker(new MarkerOptions()
                .position(Common.Kible)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.qibla_black)));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case MY_PERMİSSİON_REQUEST_CODE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (checkPlayServices()) {

                        buildGoogleApiClient();
                        createLocationRequest();
                        displayLocation();
                    }
                }
                break;
            }
        }
    }

    private void setUpLocation() {

        if (ActivityCompat.checkSelfPermission(getContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
            //Request runtime permission

            ActivityCompat.requestPermissions(getActivity(), new String[]{
                    android.Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION,
            }, MY_PERMİSSİON_REQUEST_CODE);

        } else {

            if (checkPlayServices()) {
                buildGoogleApiClient();
                createLocationRequest();
                displayLocation();
            }


        }
    }

    private void displayLocation() {

        if (ActivityCompat.checkSelfPermission(getContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (mLastLocation != null) {

            final double latitude = mLastLocation.getLatitude();
            final double longitude = mLastLocation.getLongitude();
            Common.me = new LatLng(latitude, longitude);

            LatLng latLng = new LatLng(latitude, longitude);

            showDistance(latLng);
            moveCamera(latLng, 0f);


        }


    }

    @Override
    public void onResume() {
        super.onResume();
        sensorManager.registerListener(this, compass, SensorManager.SENSOR_DELAY_NORMAL);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        sensorManager.unregisterListener(this);
    }

    @Override
    public void showDistance(LatLng latLng) {

        Location loc1 = new Location("");
        loc1.setLatitude(latLng.latitude);
        loc1.setLongitude(latLng.longitude);

        Location loc2 = new Location("");
        loc2.setLatitude(Common.Kible.latitude);
        loc2.setLongitude(Common.Kible.longitude);

        int distanceInMeters = (int)loc1.distanceTo(loc2)/1000;
        txtDistance.setText("Mesafe : "+String.valueOf(distanceInMeters)+" KM");

    }

    private void startLocationUpdates() {

        if (ActivityCompat.checkSelfPermission(getContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);

    }

    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FATEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setSmallestDisplacement(DISPLACEMENT);
    }

    private void buildGoogleApiClient() {

        mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getContext());

        if (resultCode != ConnectionResult.SUCCESS) {

            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, getActivity(), PLAY_SERVİCE_RES_REQUEST).show();
            } else {
                Toast.makeText(getContext(), "This device is not supported", Toast.LENGTH_SHORT).show();
            }
            return false;
        }
        return true;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;


    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        displayLocation();
        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        displayLocation();
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        degree = Math.round(event.values[0]);
        Common.degree = degree;

        if (degree < 0) {
            Common.degree = degree + 360f;
        }

        txtMyDegree.setVisibility(View.VISIBLE);
        txtMyDegree.setText("Benim açım: " + String.valueOf(Common.degree));

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                moveCamera(Common.me, Common.degree);

            }
        }, 3000);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
