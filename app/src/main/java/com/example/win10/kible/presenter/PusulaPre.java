package com.example.win10.kible.presenter;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;

import com.example.win10.kible.presenter.ınterface.IPusulaPre;
import com.example.win10.kible.view.IPusulaView;

public class PusulaPre implements IPusulaPre {

    IPusulaView ıPusulaView;
    SensorManager sensorManager;
    Sensor sensor;
    Activity activity;

    public PusulaPre(IPusulaView ıPusulaView, SensorManager sensorManager, Sensor compass, Activity activity) {
        this.ıPusulaView = ıPusulaView;
        this.sensorManager = sensorManager;
        this.sensor = compass;
        this.activity = activity;
    }


    @Override
    public boolean statusCompass() {

        sensorManager = (SensorManager) activity.getSystemService(Context.SENSOR_SERVICE);
        sensor = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);

        ıPusulaView.showProgress();

        if (sensor != null) {
            ıPusulaView.hideProgress();
            return true;
        } else {
            ıPusulaView.hideProgress();
            return false;
        }

    }
}
