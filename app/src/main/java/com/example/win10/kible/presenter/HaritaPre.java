package com.example.win10.kible.presenter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;


import com.example.win10.kible.R;
import com.example.win10.kible.presenter.ınterface.IHaritaPre;
import com.example.win10.kible.view.IHaritaView;

public class HaritaPre implements IHaritaPre {

    IHaritaView ıHaritaView;
    Activity activity;

    public HaritaPre(IHaritaView ıHaritaView, Activity activity) {
        this.ıHaritaView = ıHaritaView;
        this.activity = activity;
    }

    @Override
    public void statusGPS() {

        ıHaritaView.showProgress();

        final LocationManager manager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);

        if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            ıHaritaView.hideProgress();
            ıHaritaView.statusCompass();
        } else {
            setDialogGPS();
        }

    }

    @Override
    public void setDialogGPS() {

        new AlertDialog.Builder(activity, R.style.MyAlertDialogStyle)
                .setMessage("Lütfen GPS aktif hale getirin.")
                .setCancelable(false)
                .setPositiveButton("Tamam", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent callGPSSettingIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        activity.startActivity(callGPSSettingIntent);
                        ıHaritaView.hideProgress();
                    }
                })
                .setNegativeButton("Hayır", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        activity.finish();
                    }
                })
                .show();


    }

}
