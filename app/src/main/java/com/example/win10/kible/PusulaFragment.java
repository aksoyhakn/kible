package com.example.win10.kible;


import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.win10.kible.presenter.ınterface.IPusulaPre;
import com.example.win10.kible.presenter.PusulaPre;
import com.example.win10.kible.view.IPusulaView;


/**
 * A simple {@link Fragment} subclass.
 */
public class PusulaFragment extends Fragment implements SensorEventListener, IPusulaView {


    private IPusulaPre pusulaPre;
    private SensorManager sensorManager;
    private Sensor compass;
    private ImageView imgCompass;
    private TextView compassAngle;
    private float currentDegree = 0f;
    private float degree = 0f;
    private RelativeLayout relativeLayout;
    private ProgressBar pbLoad;

    public PusulaFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_pusula, container, false);
        pusulaPre = new PusulaPre(this, sensorManager, compass, getActivity());

        imgCompass = view.findViewById(R.id.imageViewCompass);
        compassAngle = view.findViewById(R.id.angle);
        relativeLayout = view.findViewById(R.id.relativeLayout);
        pbLoad = view.findViewById(R.id.pbLoad);

        sensorManager = (SensorManager) getActivity().getSystemService(Context.SENSOR_SERVICE);
        compass = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);


        if (pusulaPre.statusCompass()) {

            sensorManager.registerListener(this, compass, SensorManager.SENSOR_DELAY_NORMAL);
            imgCompass.setImageResource(R.drawable.compass);

        } else {
            imgCompass.setImageResource(R.drawable.compass2);
        }

        return view;
    }


    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }


    @Override
    public void onResume() {
        super.onResume();
        sensorManager.registerListener(this, compass, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);
    }


    @Override
    public void onSensorChanged(SensorEvent event) {
        degree = Math.round(event.values[0]);
        if (degree > 360f) {
            degree = degree - 360f;
        } else if (degree < 0) {
            degree = degree + 360f;
        }
        compassAngle.setText(" " + Float.toString(degree) + " degrees");

        RotateAnimation rotateAnimation = new RotateAnimation(currentDegree, -degree, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotateAnimation.setDuration(210);
        rotateAnimation.setFillAfter(true);

        imgCompass.startAnimation(rotateAnimation);
        currentDegree = -degree;
    }

    @Override
    public void showProgress() {
        pbLoad.setVisibility(View.VISIBLE);
        relativeLayout.setVisibility(View.INVISIBLE);
    }

    @Override
    public void hideProgress() {
        pbLoad.setVisibility(View.GONE);
        relativeLayout.setVisibility(View.VISIBLE);
    }

}
