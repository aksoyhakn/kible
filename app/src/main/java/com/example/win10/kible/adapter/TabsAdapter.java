package com.example.win10.kible.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.win10.kible.HaritaFragment;
import com.example.win10.kible.PusulaFragment;

public class TabsAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public TabsAdapter(FragmentManager fm, int NoofTabs) {
        super(fm);
        this.mNumOfTabs = NoofTabs;
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                HaritaFragment harita = new HaritaFragment();
                return harita;
            case 1:
                PusulaFragment pusula = new PusulaFragment();
                return pusula;
            default:
                return null;
        }
    }
}